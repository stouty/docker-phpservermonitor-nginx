FROM linuxserver/nginx

# set version label
LABEL maintainer="stoutyhk" version="0.2" description="phpservermonitor running behind nginx"

# environment settings
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2

EXPOSE 80 443

# add local files
COPY root/ /

VOLUME /config
